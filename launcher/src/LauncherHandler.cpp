/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 * Copyright (c) 2018,2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "LauncherHandler.hpp"

#include <functional>
#include "hmi-debug.h"

LauncherHandler::LauncherHandler(QObject *parent) :
    QObject(parent),
    mp_hs(NULL)
{

}

LauncherHandler::~LauncherHandler()
{
    if (mp_hs != NULL) {
        delete mp_hs;
    }
}

void LauncherHandler::init(int port, const char *token,
                           QLibWindowmanager *qwm,
                           QString myname) {
  mp_qwm = qwm;
  m_myname = myname;

  mp_hs = new LibHomeScreen();
  mp_hs->init(port, token);

  mp_hs->set_event_handler(
      LibHomeScreen::Event_ShowWindow, [this](json_object *object) {
        mp_qwm->activateWindow(m_myname);
      });
}
